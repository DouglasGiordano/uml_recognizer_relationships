from uml_recognizer_relationships.classifier.learning_algorithm import LearningAlgorithm

class SVM(LearningAlgorithm):

    def __init__(self, features):
        self.features = features

    def classifier(self):
